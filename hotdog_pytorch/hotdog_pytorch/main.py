from black import sys
import torch
from torch.profiler import profile, record_function, ProfilerActivity
import torch.nn.functional as F
import torchvision
from torch.utils.data import DataLoader
from torchvision import transforms
from typing import Dict, List, Tuple
from PIL import Image
from tqdm import tqdm
import os

DATA_PATH: str = "/Users/dwrodri/Datasets/hotdog/"


def prep_train_folder(
    root: str,
    filenames: List[str],
) -> List[Tuple[int, torch.Tensor]]:
    """
    take list of image files and turn into feature/label tuple
    """
    images = [Image.open(root + filename) for filename in filenames]
    label_texts = [filename.split("_")[0] for filename in filenames]
    labels = [
        torch.Tensor(
            [int(label == "hotdog" or label == "frankfurter" or label == "chili-dog")]
        ).long()
        for label in label_texts
    ]
    preprocess_pipeline = transforms.Compose(
        [transforms.ToTensor(), transforms.Resize(227), transforms.CenterCrop(225)]
    )
    return list(
        zip(labels, tqdm(map(preprocess_pipeline, images), total=len(filenames)))
    )


def prep_test_folder(root: str, filenames: List[str]) -> List[torch.Tensor]:
    preprocess_pipeline = transforms.Compose(
        [transforms.ToTensor(), transforms.Resize(227), transforms.CenterCrop(225)]
    )
    return [
        (
            root + filename,
            preprocess_pipeline(Image.open(root + filename)),
        )
        for filename in filenames
    ]


class HotDogClassifier(torch.nn.Sequential):
    def __init__(self):
        super().__init__()
        self.resnet = torchvision.models.resnet34(pretrained=True, progress=True)
        self.output = torch.nn.Linear(1000, 2)

    def forward(self, image: torch.Tensor) -> torch.Tensor:
        processed = self.resnet(image)
        return self.output(processed)


def train_loop(
    model: torch.nn.Sequential,
    loss_func: torch.nn.Module,
    loader: DataLoader,
    save: bool = True,
) -> torch.nn.Sequential:
    optimizer: torch.optim.Optimizer = torch.optim.Adam(classifier.parameters())
    i: int = 0
    with profile(activities=[ProfilerActivity.CPU], record_shapes=True) as prof:
        for label, image in loader:
            pred: torch.Tensor = classifier(image)
            loss: torch.Tensor = loss_func(pred, label.squeeze().long())

            # backprop
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # monitor progress
            if i % 10 == 0:
                print(f"Iteration {i:3d} loss:\t{loss:0.3f}")
            i += 1
        if save:
            torch.save(model.state_dict(), "hotdog.pth")
    prof.export_chrome_trace("hotdog_train_trace.json")
    return model


def val_loop(model: torch.nn.Sequential, loader: DataLoader):
    num_incorrect = 0
    num_samples = len(loader.dataset)
    with profile(activities=[ProfilerActivity.CPU], record_shapes=True) as prof:
        with torch.inference_mode():
            for labels, samples in loader:
                preds: torch.Tensor = model(samples)
                classes = torch.argmax(preds, dim=-1)
                num_incorrect += (labels.squeeze().int() ^ classes).nonzero().numel()
        print(f"Accuracy: {1.0 - (num_incorrect / num_samples):.3f} ")
    prof.export_chrome_trace("hotdog_inference_trace.json")


def test_loop(model: torch.nn.Sequential, loader: DataLoader):
    with torch.inference_mode():
        for filenames, samples in loader:
            preds: torch.Tensor = model(samples)
            classes = torch.argmax(preds, dim=-1)
            for i in range(len(filenames)):
                result = "HOT DOG" if classes[i] else "NOT A HOT DOG"
                print(f"{filenames[i]} \t {result} {classes[i]}")


if __name__ == "__main__":
    # Data setup
    train_loader = DataLoader(
        batch_size=32,
        shuffle=True,
        dataset=prep_train_folder(
            DATA_PATH + "train/", os.listdir(DATA_PATH + "train/")
        ),
    )
    val_loader = DataLoader(
        batch_size=64,
        shuffle=True,
        dataset=prep_train_folder(DATA_PATH + "val/", os.listdir(DATA_PATH + "val/")),
    )
    test_loader = DataLoader(
        batch_size=64,
        shuffle=False,
        dataset=prep_test_folder(DATA_PATH + "val/", os.listdir(DATA_PATH + "val/")),
    )

    # model + optimization setup
    classifier = HotDogClassifier()
    loss_func: torch.nn.Module = torch.nn.CrossEntropyLoss()

    num_epochs: int = 1
    for i in range(num_epochs):
        classifier = train_loop(classifier, loss_func, train_loader, save=False)

    # classifier.load_state_dict(state_dict=torch.load("hotdog.pth"))

    classifier.eval()
    test_loop(classifier, test_loader)
    val_loop(classifier, val_loader)
